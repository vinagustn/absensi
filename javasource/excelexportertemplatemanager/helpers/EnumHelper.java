package excelexportertemplatemanager.helpers;

public class EnumHelper {
	public static <T extends Enum<T>> T valueOfIgnoreCase(Class<T> enumeration, String name) {
		for (T enumValue : enumeration.getEnumConstants()) {
	        if (enumValue.name().equalsIgnoreCase(name)) { // to decide whether case sensitive or not
	            return enumValue;
	        }
	    }
		throw new IllegalArgumentException("No constant with text " + name + " found");
	}
}
